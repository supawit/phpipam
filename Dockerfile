FROM php:7.3.10-apache-stretch
ENV TZ=Asia/Bangkok

WORKDIR /var/www/html/

RUN set -x \
&& apt-get update && apt-get install -y \
libgmp-dev \
libjpeg-dev \
libpng-dev \
&& docker-php-ext-install sockets \
&& docker-php-ext-install gmp \
&& docker-php-ext-install gettext \
&& docker-php-ext-install pdo_mysql \
&& docker-php-ext-configure gd --with-png-dir=/usr --with-jpeg-dir=/usr \
&& docker-php-ext-install gd \
&& rm -rf /var/lib/apt/lists/* 

COPY phpipam/ /var/www/html/

RUN mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini"i \
&& cp config.docker.php config.php \
&& a2enmod rewrite
